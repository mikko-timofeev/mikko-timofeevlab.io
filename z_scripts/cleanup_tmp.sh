#!/bin/sh

find ./public/tmp/assets/scripts/* ! -name .keep -delete
find ./public/tmp/documents/pdf/* ! -name .keep -delete
find ./public/tmp/utils_min/js/* ! -name .keep -delete
