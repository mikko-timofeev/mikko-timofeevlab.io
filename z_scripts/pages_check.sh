#!/bin/sh

public_files=$(find public -maxdepth 1 -type f -not -name 't_*')
for file in $public_files; do
  [ -s "$file" ] || exit 1
done
exit 0
