#!/bin/sh

function qr_code_from_link {
  link="$1"
  filename=$(echo -n "$link" | md5sum | cut -d ' ' -f1)
  qrencode -l H -o "./public/images/qr/$filename.svg" "$link" -t SVG -m 0
}

langs=$(ls ./src/locales/ | cut -f1 -d'.')
for lang in $langs; do
  yq ._resources[] "./src/locales/$lang.yml" -o json -I 0 | while read res; do
    link=$(echo "$res" | jq -c '.link' | tail -c +2 | head -c -2)
    qr_code_from_link "$link"
  done

  # prefix=$(yq .meta.name "./src/locales/$lang.yml" | tr ' ' '_')
  prefix='Mikhail_Timofeev'
  page_url="${PAGE_PATH}${prefix}_-_CV_($lang).pdf"
  # echo $page_url
  qr_code_from_link "$page_url"
done
