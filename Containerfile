# https://blog.bitsrc.io/how-to-pass-environment-info-during-docker-builds-1f7c5566dd0e

ARG VERSION_ALPINE
FROM golang:alpine${VERSION_ALPINE} AS pdfcpu_current
# --- ARG variables are reset after "FROM", declare again! ---

RUN apk add --no-cache git

ARG VERSION_PDFCPU
RUN go install github.com/pdfcpu/pdfcpu/cmd/pdfcpu@v${VERSION_PDFCPU}

ARG VERSION_ALPINE
FROM alpine:${VERSION_ALPINE}
# --- ARG variables are reset after "FROM", declare again! ---

COPY --from=pdfcpu_current /go/bin/pdfcpu /usr/bin/pdfcpu

RUN apk add --no-cache bash brotli build-base curl ghostscript gzip npm sassc \
		cairo-dev gcc gdk-pixbuf gdk-pixbuf-dev git harfbuzz-dev jpeg-dev libc-dev libffi-dev libwebp libwebp-tools musl-dev pango pango-dev zlib-dev zopfli-dev \
		rustup \
		fontconfig-dev font-misc-cyrillic ttf-dejavu ttf-font-awesome \
		libqrencode jq yq
		# nginx
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 python3-dev py3-wheel \
		&& ln -sf python3 /usr/bin/python

ARG VERSION_WEASYPRINT
RUN python3 -m ensurepip \
	&& python -m pip install --upgrade pip \
	&& pip3 install --no-cache --upgrade pip python-dateutil pyyaml setuptools weasyprint==${VERSION_WEASYPRINT}
RUN npm install -g js-yaml json prettier pug static-server ts-node typescript uglify-js
RUN apk add minify --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community

ARG _UID=1000
ARG _GID=1000
ARG _UNAME=user
ARG _GNAME=user
RUN addgroup -g ${_GID} ${_GNAME} \
  && adduser -u ${_UID} -G ${_GNAME} -s /bin/bash -h /home/${_UNAME} -D ${_UNAME}

RUN mkdir /app

ENV RUSTUP_HOME=/home/${_UNAME}/.rustup CARGO_HOME=/home/${_UNAME}/.cargo

ARG VERSION_RUST
RUN rustup-init -y --profile minimal --default-toolchain=${VERSION_RUST}

ENV PATH="${RUSTUP_HOME}:/${CARGO_HOME}/bin:${PATH}"
RUN chmod -R a+w $RUSTUP_HOME $CARGO_HOME

USER ${_GID}:${_UID}
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

WORKDIR /app/
# ENTRYPOINT ["tail", "-f", "/dev/null"]
