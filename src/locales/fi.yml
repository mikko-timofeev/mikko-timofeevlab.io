---
_translations:
  key_lines:
    ago: sitten
    day: päivä
    # days_x2_x4: ...
    days: päivää
    future: tulevaisuutta
    # months_x2_x4: ...
    months: kuukautta
    remaining: vielä jäljellä
    today: tänään
    year: vuosi
    # years_x2_x4: ...
    years: vuotta
  about_page: Curriculum Vitae
  download_pdf: Ladata
  generated_on: Tämä CVen versio luotiin
  lang_native: äidinkieli
  organisation: yhdistys
  stack_summary:
    animated: TLDR
    static: avainsanoja
  dates:
    m_01s: tamm
    m_01f: tamm

    m_02s: helm
    m_02f: helm

    m_03s: maal
    m_03f: maal

    m_04s: huht
    m_04f: huht

    m_05s: touk
    m_05f: touk

    m_06s: kesä
    m_06f: kesä

    m_07s: hein
    m_07f: hein

    m_08s: elok
    m_08f: elok

    m_09s: syys
    m_09f: syys

    m_10s: loka
    m_10f: loka

    m_11s: marr
    m_11f: marr

    m_12s: joul
    m_12f: joul

    s_00: Alku
    s_since_pre: ''
    s_since_post: 'sta'
    s_until_pre: ''
    s_until_post: 'hun'
_resources:
  - &res_email
    alt: email
    picture: mails/gmail.com_nofilter
    label: '<b>mikko.timofeev</b>@gmail.com'
    link: 'mailto:mikko.timofeev@gmail.com'
    class: silent
  - &res_linkedin
    alt: resume
    picture: networks/linkedin_nofilter
    label: 'www.linkedin.com/in/<b>mikko-timofeev</b>'
    link: 'https://www.linkedin.com/in/mikko-timofeev'
    class: silent
    make_qr: true
  - &res_portfolio
    alt: portfolio
    picture: networks/gitlab_nofilter
    label: 'gitlab.com/<b>mikko-timofeev</b>'
    link: 'https://gitlab.com/users/mikko-timofeev/groups'
    make_qr: true
  # - &res_location
  #   alt: location
  #   picture: misc/location_nofilter
  #   label: Lahti, Finland
  #   link: 'geo:60.18091867019352,24.934909602821627'
  #   class: silent
  #   make_qr: true
  # - &res_birthday
  #   alt: syntymäpäivä
  #   picture: misc/bday_nofilter
  #   label: '6.6.1996'
  #   data-duration: '1996-06-06'
  - &res_xamk
    link: https://www.xamk.fi/koulutus/
    label: Mikkelin ammattikorkeakoulu
    picture: companies/xamk_nofilter
    alt: XAMKin kuva
    make_qr: true
  - &res_rosbank
    link: https://www.rosbank.ru/en/company-profile/
    label: Rosbank
    picture: companies/rosbank
    alt: Rosbankin kuva
    make_qr: true
  - &res_sympa
    link: https://www.sympa.com
    label: Sympa
    picture: companies/sympa
    alt: Logo of Sympa
    make_qr: true
  - &res_swappie
    link: https://swappie.com
    label: Swappie
    picture: companies/swappie_nofilter
    alt: Logo of Swappie
    make_qr: true
  - &res_1tv
    link: https://www.1tv.ru/
    label: Channel One
    picture: companies/1tv
    alt: Logo of Channel One
    make_qr: true
  - &res_elektakaiku
    link: https://www.elekta.com/products/life-sciences/elekta-kaiku/
    label: Elekta Kaiku
    picture: companies/elektakaiku
    alt: Logo of Elekta Kaiku
    make_qr: true
  - &res_peoples
    link: https://peoples.so/
    label: Peoples (Elecsnet)
    picture: companies/peoples
    alt: Logo of Peoples
    make_qr: true
  - &res_certificates
    picture: misc/certificates_nofilter
    alt: todistukset
    label: https://v.gd/0wj2Xp
    link: https://drive.google.com/drive/folders/0B1S8I_5EKSEMfk1HbVI4R0JYZUV3UXE2TTBtc25jMUhfdlhxUWZLbXdLbEZYYTZHZ0tpV28?resourcekey=0-MClvKvTQPFCswbbe8A2qIw
    title: Todistukset
    wid: certificates
    make_qr: true
  - &res_web_version
    alt: tämä sivu
    picture: misc/favicon_nofilter
    label: https://mikko-timofeev.gitlab.io/fi
    link: https://mikko-timofeev.gitlab.io/fi
    wclass: nodisplay
    wid: thisPage
    target: _top
    title: Verkkoversio
    make_qr: true
  - &res_bachelors
    title: Opinnäytetyö
    link: http://urn.fi/URN:NBN:fi:amk-2018060312126
    make_qr: true
meta:
  name: Mikhail Timofeev
  specialty: Web insinööri
  quote: Yritän tehdä maailmasta <u>hieman paremman</u>.
  languages:
    - _id: ru
      resource:
        picture: locales/ru
        label: RU
        class: langNative silent
        link: ./ru
    - _id: en
      resource:
        picture: locales/en
        label: EN <sub>C2</sub>
        class: langC2 silent
        link: ./en
    - _id: fi
      resource:
        picture: locales/fi
        label: FI <sub>A1</sub>
        class: langA1 silent
        link: ./fi
  properties:
    - resource: *res_email
    - resource: *res_linkedin
    - resource: *res_portfolio
data:
  qualities:
    - _id: professional
      title: Ammattilainen
      items:
        # - _id: engineer
        #   summary:
        #     header: Engineer
        #   details:
        #     list_simple:
        #       - OS administration and troubleshooting (Linux, Windows)
        #       - experienced use of IDEs (Visual Studio Code)
        #       - terminal / command prompt experience
        #       - firmware flashing / installing
        - _id: programmer
          summary:
            header: Ohjelmoija
          details:
            list_simple:
              - 'ohjelmointikielet: Python, JavaScript, Ruby'
              - 'tietokannat: PostgreSQL, MongoDB'
              # - 'testing: pytest, jest, rspec'
              - 'kontit ja podit: Docker/Podman, Kubernetes'
        # - _id: designer
        #   summary:
        #     header: Design
        #   details:
        #     list_simple:
        #       - markup (HTML, HAML) & style (CSS, BootStrap CSS)
        #       - raster editing (Adobe Photoshop, GIMP)
        #       - vector processing (Adobe Illustrator, Inkscape)
        - _id: specialist
          summary:
            header: Asiantuntija
          details:
            list_leveled:
              - title: Microsoft Certified Professional
                list_simple:
                  - Windows Operating System Fundamentals
                  - Networking Fundamentals
              - title: Cisco Certified Professional
                list_simple:
                  - CCNA Routing and switching
    - _id: teammate
      title: Joukkuekaveri
      items:
        # - _id: enthusiastic
        #   summary:
        #     header: Enthusiastic
        #   details:
        #     list_simple:
        #       - table tennis
        #       - street workout
        #       - poetry
        #       - chess
        # - _id: misc
        #   summary:
        #     header: Miscellaneous
        #   details:
        #     list_simple:
        #       - 'driving license: type B'
        # - _id: interests_personal
        #   title: ''
        #   items:
        #     - _id: interests
        #       summary:
        #         header: 'Interests'
        #       details:
        #         list_simple:
        #           - technical innovations
        #           - testing new firmware, trying new distros
        #           - performance tweaking
        #           - Rust (programming language)
        - _id: communicative
          # summary:
          #   header: Communicative
          details:
            list_simple:
              # - kohtelias, ei ristiriitainen
              - analyyttinen ajatusten jakaja
              - IT-hackathonien ja -konferenssien osallistuja
              - tuottava sekä yksin että tiimissä
    - _id: enthusiast
      title: Intoilija
      items:
        - details:
            list_simple:
              - tutustun uusimpiin ja parhaimpiin teknologioihin
              - testailen konfiguraatiomuutoksia
              - seinäkiipeily
  tools:
    title: Nykyinen tekniikkani
    tags:
      - docker
      - postgres
      - rails
      - reactjs
      - reactnative
      - typescript
  experiences:
    - _id: experienced
      title: Kokenut
      items:
        - _id: peoplesProgrammer
          summary:
            specialty: Verkko
            assignment: Ohjelmoija
            dates:
              start: 2018-08
              end: 2020-04
            languages: [venäjä, englanti]
            location: Venäjä
            resource: *res_peoples
          details:
            list_simple:
              - >
                ekosysteemi | Docker, Ruby on Rails, ReactJS, Python,
                RabbitMQ, MongoDB
          tags:
            - docker
            - elasticsearch
            - gitlabci
            - mongodb
            - rabbitmq
            - rails
            - reactjs
            - ruby
            - shell
        - _id: channelOneDeveloper
          summary:
            specialty: Verkko
            assignment: Kehittäjä
            dates:
              start: 2020-04
              end: 2021-04
            languages: [venäjä]
            location: Venäjä
            resource: *res_1tv
          details:
            list_simple:
              - >
                verkkosivuston kehitys, monoliitin APIsaatio
              - endpointin määrittely Swaggerissa
          tags:
            - docker
            - elasticsearch
            - gitlabci
            - postgres
            - rabbitmq
            - rails
            - ruby
            - shell
            - swagger
        - _id: channelOneDevOps
          summary:
            assignment: DevOps Insinööri
            dates:
              start: 2021-04
              end: 2022-02
            languages: [venäjä]
            location: Venäjä
            resource: *res_1tv
          details:
            list_simple:
              - >
                verkkosivuston konfigurointi ja tuki
              - konttityökalut ja projektinhallintaskriptit
          tags:
            - docker
            - gitlabci
            - kubernetes
            - postgres
            - python
            - rabbitmq
            - elasticsearch
            - shell
        - _id: swappieSoftwareEngineer
          summary:
            assignment: Ohjelmistoinsinööri
            dates:
              start: 2022-03
              end: 2022-06
            languages: [englanti]
            location: Suomi
            resource: *res_swappie
          details:
            list_simple:
              - Django ja ReactJS (Typescript) -kehitys
              - CI/CD-kulku Google Cloudin avulla
              - konttien debuggaus (Docker)
          tags:
            - django
            - docker
            - gcloud
            - kubernetes
            - python
            - reactjs
            - shell
            - typescript
        - _id: sympaFullstackDeveloper
          summary:
            assignment: Full-stack Kehittäjä
            dates:
              start: '2022-08-01'
              end: '2023-07-12'
            languages: [englanti]
            location: Suomi
            resource: *res_sympa
          details:
            list_simple:
              - tuotekehitys
              - tuotantoympäristön uusiminen
          tags:
            - azure
            - dotnet
            - docker
        - _id: elektaSoftwareEngineer
          summary:
            assignment: Software Engineer
            dates:
              start: '2023-10-02'
            languages: [English]
            location: Finland
            resource: *res_elektakaiku
          details:
            list_simple:
              - full-stack development
              - mobile development
          tags:
            - docker
            - rails
            - reactjs
            - reactnative
    - _id: educated
      title: SIVISTYNYT
      items:
        - _id: rosbank
          summary:
            assignment: DevOps Harjoittelija
            dates:
              start: 2017-08
              end: 2017-12
            languages: [venäjä, englanti]
            location: Venäjä
            resource: *res_rosbank
          details:
            list_simple:
              - >
                työskentelin pankin IT-järjestelmän pakettien kanssa
            list_linked:
              - title: >
                  automation-maven-pluginin kehitys - IBM BAR tiedoston
                  muodostaminen, päivittäminen ja käyttöönotto (Opinnäytetyö)
                resource: *res_bachelors
          # tags:
          #   - ibm
          #   - java
          #   - maven
          #   - oracledbms
          #   - oraclesql
          #   - shell
        - _id: xamk
          summary:
            specialty: Tietotekniikka
            assignment: Insinööri
            dates:
              start: 2014-08
              end: 2018-05
            languages: [englanti]
            location: Suomi
            resource: *res_xamk
          details:
            list_simple:
              - tietokoneavustaja HW/SW taitoja
              - LAN & WAN johtaja
              - GIS and 3D Modeling
              - >
                AV-tuotannot: monikameratuotanto
                kokemus, video- ja äänieditointi
          # tags:
          #   - cisco
          #   - microsoft
          #   - networking
          #   - avproductions

  tag_pictures:
    dotnet: dotnet_nofilter
    reactnative: reactnative_nofilter

  resources:
    - resource: *res_certificates
    - resource: *res_web_version
