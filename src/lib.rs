extern crate console_error_panic_hook;

use chrono::{NaiveDate, Utc};
use regex::Regex;
use wasm_bindgen::prelude::*;

// === Make JS methods callable from within Rust: ===

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

/* This library counts time difference in years between two dates
 *
 * Event declaration types:
 *  fixed term: "A,B" where both can be Y-m-d, Y-m or Y
 *  not yet over: "A," where A can be Y-m-d, Y-m or Y
 *  without known start date: ",B" where Y can be Y-m-d, Y-m or Y
 *
 * ",B" returns duration from today until date
 *
 * Roundups and precision:
 *  A without d turns into Y-m-01
 *  A without m turns into Y-01-01
 *  B without d turns into Y-m-28
 *  B without m turns into Y-12-28
 *
 * Edge case:
 *  date A in the future - return difference as negative float
 */

fn round_date(naive_partial_iso_date: &str, roundup: bool) -> String {
    let parts = naive_partial_iso_date.split("-").collect::<Vec<_>>();
    let mut size = parts.len();

    if size > 1 {
        if parts[1] == "" {
            size = 1;
        } else {
            if size == 3 {
                if parts[2] == "" {
                    size = 2;
                } else {
                    return format!("{naive_partial_iso_date}");
                }
            }
        }
    }

    if roundup {  // to end of month, year
        if size == 2 {  // Y-m
            return format!("{}-{}-28", parts[0], parts[1]);
        } else {  // Y
            return format!("{}-12-28", parts[0]);
        }
    } else {  // to beginning of month, year
        if size == 2 {  // Y-m
            return format!("{}-{}-01", parts[0], parts[1]);
        } else {  // Y
            return format!("{}-01-01", parts[0]);
        }
    }
}

#[test]
fn test_round_date() {
    assert_eq!("1996-06-06", round_date("1996-06-06", false));

    // A
    assert_eq!("1996-06-01", round_date("1996-06", false));
    assert_eq!("1996-01-01", round_date("1996", false));

    // B
    assert_eq!("1996-06-28", round_date("1996-06", true));
    assert_eq!("1996-12-28", round_date("1996", true));
}


fn period_duration_in_days(date_a: NaiveDate, date_b: NaiveDate) -> i64 {
    if date_a < date_b {  // started in the past
        return date_b.signed_duration_since(date_a).num_days();
    } else {  // starts in the future
        return - date_a.signed_duration_since(date_b).num_days();
    };
}

#[test]
fn test_period_duration_in_days() {
    assert_eq!(0, period_duration_in_days(
        NaiveDate::from_ymd_opt(1996, 06, 06).unwrap(),
        NaiveDate::from_ymd_opt(1996, 06, 06).unwrap()
    ));

    assert_eq!(3, period_duration_in_days(
        NaiveDate::from_ymd_opt(1996, 06, 06).unwrap(),
        NaiveDate::from_ymd_opt(1996, 06, 09).unwrap()
    ));

    assert_eq!(-5, period_duration_in_days(
        NaiveDate::from_ymd_opt(1996, 06, 06).unwrap(),
        NaiveDate::from_ymd_opt(1996, 06, 01).unwrap()
    ));
}


fn is_valid_partial_iso_date(potentially_valid_iso_date: &str) -> bool {
    let re = Regex::new(r"^\d{4}-\d{2}-\d{2}|\d{4}-\d{2}|\d{4}$").unwrap();
    return re.is_match(potentially_valid_iso_date);
}

#[test]
fn test_is_valid_partial_iso_date() {
    assert_eq!(true, is_valid_partial_iso_date("1996"));
    assert_eq!(true, is_valid_partial_iso_date("1996-06"));
    assert_eq!(true, is_valid_partial_iso_date("1996-06-06"));
}


// === Make Rust methods available at JS level in webapp: ===

#[wasm_bindgen]
pub fn duration(timeline: &str) -> f32 {
    console_error_panic_hook::set_once();

    let dates = timeline.split(",").collect::<Vec<_>>();
    let size = dates.len();

    let mut date_a @ mut date_b = Utc::now().naive_utc().date();

    // round first date down
    if is_valid_partial_iso_date(dates[0]) {
        date_a = NaiveDate::parse_from_str(
            &round_date(dates[0], false), "%Y-%m-%d"
        ).unwrap();
    }

    // round second date up
    if size > 1 && is_valid_partial_iso_date(dates[1]) {
        date_b = NaiveDate::parse_from_str(
            &round_date(dates[1], true), "%Y-%m-%d"
        ).unwrap();
    }

    return period_duration_in_days(date_a, date_b) as f32 / 365.0;
}
