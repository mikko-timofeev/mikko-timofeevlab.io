// https://developer.mozilla.org/en-US/docs/Web/API/CustomElementRegistry/define

window.addEventListener(
  'load',
  () => {
    // --- interfaces

    interface Translator {
      _translationCache: Record<string, string | undefined>
      readonly get: Function
    }

    // --- methods

    const addEventListenerToNodeList = (
      nodes: NodeListOf<Element>,
      eventName: string,
      fun: Function
    ) => {
      nodes.forEach(node => {
        node.addEventListener(
          eventName,
          (e: Event) => {
            fun(node, e)
          },
          false
        )
      })
    }

    const tryStackHighlighter = (tag: string) => {
      document.querySelectorAll(`[data-filtered-display]`).forEach(x => {
        // @ts-ignore
        x.removeAttribute('data-filtered-display')
      })

      // @ts-ignore
      document.querySelector('main').removeAttribute('data-filtered-view')

      if (tag === '' || tag === '#') return
      tag = tag.substring(1)

      const tagSelector = document.querySelector(`#${tag}.keyword`)
      if (tagSelector) {
        // @ts-ignore
        document
          .querySelector('main')
          .setAttribute('data-filtered-view', 'true')

        document.querySelectorAll(`[data-tags~=${tag}]`).forEach(x => {
          // @ts-ignore
          x.setAttribute('data-filtered-display', 'true')
        })

        document
          .querySelectorAll(`[data-tags]:not([data-tags~=${tag}])`)
          .forEach(x => {
            // @ts-ignore
            x.setAttribute('data-filtered-display', 'false')
          })

        // @ts-ignore
        document.getElementById('tagsStatic').checked = true
      }

      // const spoilerSelector = document.querySelector(
      //   `#${tag}[data-toggleable='true']`
      // )
      // if (spoilerSelector) {
      //   spoilerSelector.setAttribute('open', 'open')
      // }
    }

    const share = (el: HTMLInputElement) => {
      const link = el.getAttribute('data-link') || ''

      if (typeof navigator.share === 'undefined' || !navigator.share) {
        // console.log('Copied to clipboard')
        navigator.clipboard.writeText(link)
      } else {
        const title = el.getAttribute('data-title') || ''
        const label = el.getAttribute('aria-label') || title

        navigator
          .share({
            title: title,
            text: label,
            url: link
          })
          .then(() => {
            // console.log('Share complete')
          })
        el.blur()
      }
    }

    // --- tools

    const t: Translator = {
      _translationCache: {},
      get(key: string) {
        if (key in this._translationCache) {
          return this._translationCache[key]
        }
        return (
          (<HTMLInputElement>document.querySelector(`#t_${key}`))?.value || key
        )
      }
    }

    // ---

    // @ts-ignore
    document.querySelector('#resetTags').addEventListener('click', () => {
      window.history.replaceState(null, '', '#')
      window.history.pushState(null, '', '#tags')
      history.back()
    })

    addEventListenerToNodeList(
      document.querySelectorAll('a[href^="#"]'),
      'click',
      (targetEl: HTMLAnchorElement, setHashAndScrollToElement: Event) => {
        // unless link is the same (when link is valid)
        if (
          !(
            targetEl instanceof HTMLElement &&
            targetEl.href.split('#')[1] !== window.location.hash.split('#')[1]
          )
        ) {
          setHashAndScrollToElement.preventDefault()
          window.history.replaceState(null, '', '#')

          // gently force css :target change
          window.history.pushState(null, '', '#')
          history.back()

          tryStackHighlighter('')
        }
      }
    )

    // addEventListenerToNodeList(
    //   document.querySelectorAll('details[data-toggleable="false"]'),
    //   'toggle',
    //   (_: HTMLDetailsElement, toggleEvent: Event) => {
    //     toggleEvent.preventDefault()
    //     console.log('Preventing collapse o')
    //   }
    // )

    addEventListenerToNodeList(
      document.querySelectorAll('.share[data-link]:not([data-link=""])'),
      'focus',
      (targetEl: HTMLInputElement, toggleEvent: Event) => {
        share(targetEl)
      }
    )

    // addEventListenerToNodeList(
    //   document.querySelectorAll('details[data-toggleable="true"][id]'),
    //   'toggle',
    //   (_: HTMLDetailsElement, toggleEvent: Event) => {
    //     toggleEvent.preventDefault()

    //     const newHash =
    //       '#' +
    //       // @ts-ignore
    //       (toggleEvent.target.getAttribute('open') === ''
    //         ? // @ts-ignore
    //           toggleEvent.target.getAttribute('id')
    //         : '')

    //     window.history.replaceState(null, '', newHash)
    //     window.history.pushState(null, '', newHash)
    //     // tryStackHighlighter(newHash)
    //   }
    // )

    tryStackHighlighter(window.location.hash)

    window.addEventListener('hashchange', () => {
      tryStackHighlighter(window.location.hash)
    })

    const wasmLoading = setInterval(() => {
      if (globalThis.hasOwnProperty('duration')) {
        clearInterval(wasmLoading)

        document.querySelectorAll('[data-duration]').forEach(x => {
          let rawDuration = x.getAttribute('data-duration') || ''
          if (rawDuration.indexOf(',') == -1) rawDuration = rawDuration + ','

          // @ts-ignore
          let durationInYears = globalThis.duration(rawDuration)
          x.removeAttribute('data-duration')

          let future = durationInYears < 0
          if (future) {
            durationInYears *= -1.0
          }

          let durationLabel: string
          if (durationInYears === 0) {
            durationLabel = t.get('future')
          } else if (durationInYears < 1) {
            // several months
            durationLabel = `≈ ${(durationInYears * 12).toFixed()} ${t.get(
              'months'
            )}`
          } else {
            const roundedDurationInYears = Math.round(durationInYears)
            if (roundedDurationInYears % 10 !== 1) {
              durationLabel = `≈ ${roundedDurationInYears} ${t.get('years')}`
            } else {
              durationLabel = `≈ 1 ${t.get('year')}`
            }

            if (future) durationLabel = durationLabel + ' ' + t.get('remaining')
          }

          // @ts-ignore
          x.title = durationLabel
        })

        const cvAge = document.querySelector('#dateGen [data-event-day]')
        const durationInDays = parseInt(
          // @ts-ignore
          globalThis.duration(cvAge.getAttribute('data-event-day')) * 365
        )

        let durationLabel: string

        if (durationInDays === 0) {
          durationLabel = t.get('today')
        } else {
          durationLabel =
            durationInDays +
            (durationInDays % 10 !== 1
              ? ` ${t.get('days')}`
              : ` ${t.get('day')}`) +
            ` ${t.get('ago')}`
        }
        // @ts-ignore
        cvAge.title = durationLabel
      }
    }, 2000)
  },
  {
    once: true
  }
)
